package com.techelevator.shapes;

public class Circle implements Shape {

	private double radius;

	public Circle(double radius) {
		this.radius = radius;
	}

	public String getName() {
		return "Circle";
	}

	public double getArea() {
		return Math.PI * Math.pow(this.radius, 2);
	}

	public double getRadius() {
		return radius;
	}

}
