package com.techelevator.shapes;

public interface Shape {

	String getName();

	double getArea();

}
