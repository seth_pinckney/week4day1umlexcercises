package com.techelevator.shapes;

import com.techelevator.shapes.Circle;
import com.techelevator.shapes.Rectangle;

public class ShapeTester {

	public static void main(String[] args) {

		Circle testCircle = new Circle(10);
		System.out.println(testCircle.getArea());
		System.out.println(testCircle.getRadius());
		System.out.println(testCircle.getName());

		Rectangle testRectangle = new Rectangle(10, 20);
		System.out.println(testRectangle.getArea());
		System.out.println(testRectangle.getLength());
		System.out.println(testRectangle.getWidth());
		System.out.println(testRectangle.getName());

		Square testSquare = new Square(20);
		System.out.println(testSquare.getName());
		System.out.println(testSquare.getArea());

	}
}
