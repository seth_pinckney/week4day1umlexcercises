package com.techelevator.shapes;

public class Square extends Rectangle {

	private String name = "Square";

	public Square(int sideLength) {
		super(sideLength, sideLength);

	}

	public String getName() {

		return name;

	}

}
