package com.techelevator.shapes;

public class Rectangle implements Shape {

	private int length;
	private int width;

	public Rectangle(int length, int width) {
		this.length = length;
		this.width = width;
	}

	public String getName() {
		return "Rectangle";
	}

	public double getArea() {
		return length * width;
	}

	public int getLength() {
		return length;
	}

	public int getWidth() {
		return width;
	}

}
