package com.techelevator.person;

	public abstract class Person {

		public String firstName;
		public String lastName;
	
		public Person(String firstName, String lastName) {
			System.out.println("***Person Constructor");
			this.firstName = firstName;
			this.lastName = lastName;
			
		}
	
		public String getFirstName() {
				System.out.println("***Person.getFirstName()");
				return firstName;
		}
		
		
		public String getLastName() {
			System.out.println("***Person.getLastName()");
			return lastName;
		}
	
		
		public String getFullName() {
			System.out.println("***Person.getFullName()");
			return firstName + " " + lastName;
		}
		
}
