package com.techelevator.person;

public class PersonTester {

	public static void main(String[] args) {
		
	
		Teacher JohnDoe = new Teacher("John", "Doe", 15);

		System.out.println(JohnDoe.getFullName());
		System.out.println(JohnDoe.yearsOfService);
		System.out.println(JohnDoe.isTenured());
		
	}
}