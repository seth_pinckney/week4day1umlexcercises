package com.techelevator.person;

public class Teacher extends Employee {

	public Teacher(String firstName, String lastName, int yearsOfService) {
		super(firstName, lastName, yearsOfService);
	}

	public boolean isTenured() {
		return this.yearsOfService > 3;
	}
	
}
