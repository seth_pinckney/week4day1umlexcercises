package com.techelevator.person;

public abstract class Employee extends Person {

	int yearsOfService;
	
	public Employee(String firstName, String lastName, int yearsOfService) {
		super(firstName, lastName);
	
	}

	
	public int getYearsOfService() {
		return this.yearsOfService;
	}
	
}
